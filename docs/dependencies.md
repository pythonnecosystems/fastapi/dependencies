# 종속성

## "동속성 주입"이란?
"**종속성 주입**"이란 프로그래밍에서 코드(이 경우 *경로 연산 함수*)가 "종속성"을 작동하고 사용하는 데 필요한 것들을 선언하는 방법을 의미한다.

그런 다음 해당 시스템(이 경우 **FastAPI**)이 코드에 필요한 종속성을 제공하는 데 필요한 모든 작업을 처리합니다(종속성 "주입").

이 기능은 다음과 같은 경우에 매우 유용하다.

- 로직 공유(동일한 코드 로직을 반복해서 사용).
- 데이터베이스 연결 공유.
- 보안, 인증, 역할 요구 사항 등에 적용.
- 그리고 다른 많은 경우...

코드 반복을 최소화하면서 이 모든 것이 가능하다.

## 첫 단계
아주 간단한 예를 들어 보겠다. 너무 간단해서 지금은 그다지 유용하지 않을 것이다.

그러나 이렇게 하면 종속성 주입 시스템이 어떻게 작동하는지에 집중할 수 있다.

### 종속성 생성 또는 "종속 가능한"
먼저 종속성에 대해 살펴보자.

*경로 연산 함수*는 받을 수 있는 모든 동일한 매개변수를 받을 수 있는 함수일 뿐입니다.

**Python3.10+**

```python
from typing import Annotated

from fastapi import Depends, FastAPI

app = FastAPI()


async def common_parameters(q: str | None = None, skip: int = 0, limit: int = 100):
    return {"q": q, "skip": skip, "limit": limit}


@app.get("/items/")
async def read_items(commons: Annotated[dict, Depends(common_parameters)]):
    return commons


@app.get("/users/")
async def read_users(commons: Annotated[dict, Depends(common_parameters)]):
    return commons
```

여기까지

**2줄**

그리고 모든 *경로 연산 함수*와 동일한 모양과 구조를 가지고 있다.

'데코레이터'가 없는 *경로 연산 함수*라고 생각하면 된다(`@app.get("/some-path")` 제외).

그리고 원하는 것은 무엇이든 반환할 수 있다.

이 경우 이 종속성은 다음과 같은 것을 기대한다.

- 선택적 쿼리 매개변수 `str`타입인 `q`.
- 선택적 쿼리 매개변수 `int` 타입이거나 기본 값이 `0`인 `skip`.
- 선택적 쿼리 매개 변수 `int` 타입이거나 기본 값이 `100`인 `limit`.

그런 다음 해당 값이 포함된 딕셔너리를 반환한다.

> **Info**<br>
> FastAPI는 버전 0.95.0에서 `Annotated` 기능을 추가하고 이를 권장하기 시작했다.
> 
> 이전 버전을 사용하는 경우 `Annotated`을 사용하려고 할 때 오류가 발생할 수 있다.
> 
> 주석을 사용하기 전에 [FastAPI 버전을 0.95.1 이상으로 업그레이드](https://fastapi.tiangolo.com/deployment/versions/#upgrading-the-fastapi-versions)해야 한다.

### `Depends` 임포트

**Python3.10+**

```python
from typing import Annotated

from fastapi import Depends, FastAPI

app = FastAPI()


async def common_parameters(q: str | None = None, skip: int = 0, limit: int = 100):
    return {"q": q, "skip": skip, "limit": limit}


@app.get("/items/")
async def read_items(commons: Annotated[dict, Depends(common_parameters)]):
    return commons


@app.get("/users/")
async def read_users(commons: Annotated[dict, Depends(common_parameters)]):
    return commons
```

### "Dependant"에서 종속성 선언
*경로 연산 함수* 매개변수에 `Body`, `Query` 등을 사용하는 것과 같은 방식으로 새 매개변수에 `Depends`을 사용한다.

**Python3.10+**

```python
from typing import Annotated

from fastapi import Depends, FastAPI

app = FastAPI()


async def common_parameters(q: str | None = None, skip: int = 0, limit: int = 100):
    return {"q": q, "skip": skip, "limit": limit}

@app.get("/items/")
```
<span style='background-color:#F7DDBE'>
```python
async def read_items(commons: Annotated[dict, Depends(common_parameters)]):
```
</span>

```python
    return commons

@app.get("/users/")
```

<span style='background-color:#F7DDBE'>
```python
async def read_users(commons: Annotated[dict, Depends(common_parameters)]):
```
</span>

```python
    return commons
```

`Body`, `Query` 등을 사용하는 것과 같은 방식으로 함수의 매개변수에 `Depends`를 사용하지만, `Depends`는 약간 다르게 작동한다.

`Depends`에게 하나의 매개변수만 제공한다.

이 매개변수는 함수와 같은 것이어야 한다.

**직접 호출하지 않고**(끝에 괄호를 추가하지 않음) `Depends()`에 매개변수로 전달하기만 하면 된다.

그리고 이 함수는 *경로 연산 함수*와 같은 방식으로 매개 변수를 받는다.

> **Tip**<br>
> 앞으로 함수 외에 어떤 다른 '사물'을 종속성으로 사용할 수 있는지 살펴보겠다.

새 요청이 도착할 때마다 **FastAPI**가 처리한다.

- 바른 매개변수를 사용하여 종속성("의존성") 함수를 호출한다.
- 함수에서 결과를 가져온다.
- 그 결과를 *경로 연산 함수*의 매개변수에 할당한다.

![](./images/screen_shot_01.png)

이렇게 하면 공유 코드를 한 번만 작성하면 *경로 연산*을 위한 호출은 **FastAPI**가 알아서 처리한다.

> **Check**<br>
> 특별한 클래스를 생성하고 이를 **FastAPI**에 전달하여 'register' 또는 이와 유사한 작업을 할 필요가 없다.
> 
> `Depends`에 전달하기만 하면 나머지는 **FastAPI**가 알아서 처리한다.

## `Annotated` 종속성 공유
위의 예시에서 약간의 **코드 중복**이 있음을 알 수 있다.

`common_parameters()` 종속성을 사용해야 하는 경우, 타입 어노테이션과 `Depends()`를 사용하여 전체 매개변수를 작성해야 한다:

```python
commons: Annotated[딕셔너리, Depends(common_parameters)]
```

하지만 `Annotated`를 사용하기 때문에 해당 `Annotated` 값을 변수에 저장하고 여러 곳에서 사용할 수 있다.

```python
from typing import Annotated

from fastapi import Depends, FastAPI

app = FastAPI()


async def common_parameters(q: str | None = None, skip: int = 0, limit: int = 100):
    return {"q": q, "skip": skip, "limit": limit}


CommonsDep = Annotated[dict, Depends(common_parameters)]


@app.get("/items/")
async def read_items(commons: CommonsDep):
    return commons


@app.get("/users/")
async def read_users(commons: CommonsDep):
    return commons
```

> **Tip**<br>
> 이것은 "type alias"라고 하는 표준 Python이며, 실제로는 **FastAPI**에만 국한된 것이 아니다.
> 
> 하지만 **FastAPI**는 `Annotated`된 것을 포함한 Python 표준을 기반으로 하기 때문에 코드에서 이 트릭을 사용할 수 있다. 😎

종속성은 예상대로 계속 작동하며, **가장 좋은 점은 타입 정보가 보존되어** 에디터에서 **자동 완성**, **인라인 오류** 등을 계속 제공할 수 있다는 것이다. `mypy`와 같은 다른 도구도 마찬가지이다.

이 기능은 **많은 경로 작업**에서 **동일한 종속성**을 반복해서 사용하는 **대규모 코드 베이스**에서 사용할 때 특히 유용하다.


## `async` 또는 `async` 하지 않기
종속성 역시 *경로 연산 함수*와 동일하게 **FastAPI**에 의해 호출되므로 함수를 정의할 때 동일한 규칙이 적용된다.

`async def` 또는 일반 `def`를 사용할 수 있다.

그리고 일반 `def` *경로 연산 함수* 안에 `async def`로 종속성을 선언하거나, `async def` *경로 연산 함수* 안에 `def` 종속성을 선언하는 등의 방법으로 종속성을 선언할 수 있다.

상관없다. **FastAPI**는 무엇을 해야 할지 알고 있다.

> **Note**<br>
> 모르는 경우, 문서의 `async`와 `await`에 대하여 [`Async: "in a hurry?`](https://fastapi.tiangolo.com/async/)에서 확인하세요.

## OpenAPI와 통합
종속성(및 하위 종속성)에 대한 모든 요청 선언, 검증 및 요구 사항은 동일한 OpenAPI 스키마에 통합된다.

따라서 대화형 문서에는 다음 종속성의 모든 정보도 포함된다.

![](./images/image01.png)

## 간단한 사용법
살펴보면 경로와 연산이 일치할 때마다 *경로 연산 함수*를 사용한다고 선언하고, 이후 **FastAPI**가 정확한 파라미터로 함수를 호출하는 과정을 거쳐 요청에서 데이터를 추출한다.

실제로 모든 웹 프레임워크(또는 대부분)는 이러한 방식으로 작동합니다.

그런 함수들을 직접 호출하는 것은 결코 아니다. 프레임워크(이 경우 **FastAPI**)에 의해 호출된다.

또한 Dependency Injection 시스템을 사용하면 **FastAPI**에게 *경로 운영 함수*이 *경로 운영 함수* 이전에 실행되어야 하는 다른 것에도 "의존"한다고 말할 수 있으며, **FastAPI**가 이를 실행하고 결과를 "인젝션"하는 것을 처리할 것이다.

이와 같은 "종속성 주입"과 동일한 개념의 다른 일반적인 용어들은 다음과 같다.

- 자원
- 제공자
- 서비스
- 주사제
- 구성 요소들

## **FastAPI** 플러그인
**종속성 주입** 시스템을 사용하면 통합과 "플러그인"을 만들 수 있다. 그러나 사실 "플러그인"을 만들 필요가 없다, 종속성을 사용하면 *경로 연산 함수*에 사용할 수 있는 무한한 수의 통합과 상호 작용을 선언할 수 있기 때문이다.

그리고 매우 간단하고 직관적인 방법으로 종존성을 만들 수 있어 필요한 Python 패키지를 임포트하여 말 그대로 두 줄의 코드로 API 기능과 통합할 수 있다.

관계형 NoSQL 데이터베이스, 보안 등에 대한 예를 찾아 볼 수 있다.

## **FastAPI** 호환성
종속성 주입 시스템의 단순성으로 인해 **FastAPI**는 다음과 호환된다.

- 모든 관계형 데이터베이스
- NoSQL 데이터베이스
- 외부 패키지
- 외부 API
- 인증 및 권한 시스템
- API 사용 모니터링 시스템
- 응답 데이터 주입 시스템
- 기타.

## 간단하고 강력한 기능
계층적 종속성 주입 시스템은 정의와 사용이 매우 간단하지만 여전히 매우 강력하다.

종속성을 정의하여 종속성 자체를 정의할 수 있다.

결국, 종속성의 계층적 트리가 구축되고, **종속성 주입 시스템**은 여러분(및 그 하위 종속성)을 위해 이 모든 종속성을 해결하고 각 단계에서 결과를 제공(주입)하는 것을 담당한다.

예를 들어, 다음과 같이 4 API 엔트포인트(*경로 작업*)가 있다고 가정해 보자.

- `/items/public/`
- `/items/private/`
- `/users/{user_id}/`
- `/items/pro/`

그런 다음 종속성과 하위 종속성만으로 각 항목에 대해 다른 권한 요구 사항을 추가할 수 있다.

![](./images/screen_shot_02.png)

## **OpenAPI**와 통합
이러한 모든 종속성은 요구 사항을 선언하는 동시에 매개 변수, 검증 등을 *경로 작업*에 추가합니다.

**FastAPI**는 OpenAPI 스키마에 이 모든 것을 추가하여 대화형 문서 시스템에 표시할 수 있도록 관리한다.
