# 종속성 <sup>[1](#footnote_1)</sup>

**FastAPI**는 매우 강력하면서도 직관적인 **종속성 주입(dependency Injection)** 시스템을 갖추고 있다.

사용이 매우 간단하고 모든 개발자가 다른 컴포넌트를 **FastAPI**와 매우 쉽게 통합할 수 있도록 설계되었습니다.

<a name="footnote_1">1</a>: 이 페이지는 [Dependencies](https://fastapi.tiangolo.com/tutorial/dependencies/)를 편역하였다.
